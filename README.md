# DIY Fantasy choose-your-own-adventure / spellbook

A gift for my DnD players. After putting up with my terrible DMing for an entire year, it only feels appropriate to make them something nice.

![Main Image](/images/main.jpg)

## Requirements
- Paint and Varnish
- Epoxy Resin / Acrylic sheets depending on how you want to fill the inner section (see Inner point in Limitations)
- Solder, wires, 330 ohm resistor (optional)
- A 3D printer
- Arduino Uno
- 2.8" TFT screen / Touchscreen / SD-card reader combo
- SD Card
- Neopixels / WS2812Bs (4 LEDs fit though your strip may differ)

## How to make
1. Print the `.stl` files in the `3dmodels` directory. The `.blend` files are the original drawings (inspired by https://www.thingiverse.com/thing:2064476).
2. Copy the .txt files in the `pages` directory to the root of the SD card.
3. Put the SD Card in the TFT shield reader
4. Solder the necessary wires from the Neopixels / WS2812b to the Arduino. Specifically, this means connect `5v` and `GND` to the relevant pin on the Arduino. Connect `Data` to `D1` on the Arduino (using the 330 ohm resistor if desired).
5. Connect the TFT shield to the Arduino.
6. Upload the code (`.ino` files) to the Arduino (https://www.arduino.cc/en/Tutorial/getting-started-with-ide-v2/ide-v2-uploading-a-sketch)

![Wiring Image](/images/wiring.jpg)

### Features of this particular story and implementation (though you can easily change the story)
- Many win conditions!
- Many deaths!
- Choices that affect the ending and the results of fights!
- Dodgy code and wiring that may burn the house down!
- Feel like you're really using all the Arduino you paid for with only one free GPIO port and 2% remaining memory!
- A confused narrator who keeps shifting between first and third person because I spent months trying to fix one particular problem so the actual writing took a back-seat!
- Any many many more (like 2 whole other points)!

## Limitations, Problems, and Potential Upgrades
- Originally the story was written in PROGMEM but that quickly used up all the space. I had to offload all the text on to the SD Card. There's probably a much more efficient way of writing this code which frees up a lot of memory but I am not that good.
- A VERY weird bug happens when connecting the LEDs (or rather, sending signals to the LED strip). When the LEDs are not connected or only powered (meaning no data signal transmitting) then everything works fine. When the LEDs get used, the touchscreen receives spurious data. This causes the program to think that a touch has been pressed but no such event has occurred. It's weird and I can't work it out. I've tried putting a diode on the data line, a resistor, changing the digital pin (as I'm using PIN 1 which is a serial port (helpful for debugging if I can't read the serial output because the LEDs are in use...)) and nothing worked. In the end I put a huge constraint on the Z pressure reading. This means the spurious data still occurs but very rarely triggers the page turn whereas an actual touch does still work (with some wiggling).
- Epoxy - The original idea was to fill the inner section with epoxy with swirled paint at different layers for a 3d effect. I did this and it did look ok but looks were about all that was passable about it. I am terrible with epoxy. That was certainly an idea that was better in my head. If someone has proper moulds and a decent way to decorate it then it'll likely be a lot better. Using clay as a redneck-engineering mould was not the best idea...<br />
As such, I swapped this out for a sheet of acrylic with a sticker on top of it. The effect isn't as "magical" as I'd hoped but it's good enough.
- In retrospect, a grille or swirl pattern instead of an epoxy fill would have been better. The epoxy makes sense from a "it'll look magical" perspective but it takes so much extra work. If you get the ratio wrong, it's ruined. If you don't mix it enough, it's ruined. If you get the paint pattern wrong, it's ruined. To say nothing of actually making the mould for the pour. Better to just have a grille and let the LEDs do the work with an added diffuser.
- Setting this up with an ESP8266 or other wifi device may be overkill but it would allow a lot more freedom with changing the story, adding images, controlling the LEDs, etc.
- I dislike the way pages are read. The program doesn't like reading entire lines at a time (there appears to be a 500 character limit though I was often hitting 200ish limits due to the Arduino's low memory) so I changed it to output characters directly so the buffer only had one `char` in it at a time. As such, the `.txt` files are pretty difficult to read and hard to guess at which line number represents which condition.