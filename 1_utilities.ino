#include <avr/wdt.h>

// Assign human-readable names to some common 16-bit color values:
#define BLACK   0x0000
#define BLUE    0x001F
#define RED     0xF800
#define GREEN   0x07E0
#define CYAN    0x07FF
#define MAGENTA 0xF81F
#define YELLOW  0xFFE0
#define WHITE   0xFFFF

void reboot() {
  wdt_disable();
  wdt_enable(WDTO_15MS);
  while (1) {}
}

void reset() {
  tft.fillScreen(BLACK);
}

void displayFromSD(String filename_without_extension, unsigned int lineNumber, unsigned int colour = WHITE, unsigned int text_size = 1) {

  tft.setTextColor(colour);
  tft.setTextSize(text_size);

  File file = SD.open(filename_without_extension + ".txt");
  file.seek(0);

  unsigned int line = 1;
  while (file.available()) {

    char character = (char) file.read();

    if (line == lineNumber) {
      if (character == '\n') break;
      tft.print(character);
    }
    if (character == '\n') {
      line++;
    }
  }

  tft.println("\n");
  file.close();

}

void write(String text, unsigned int colour = WHITE, int text_size = 1) {
  tft.setTextColor(colour);
  tft.setTextSize(text_size);
  tft.println(text);
  tft.println("");
}

void drawOptions(String _file, unsigned int _line1, unsigned int _line2) {
  tft.fillRect(OPTION1_X, OPTIONS_Y, BOXSIZE, BOXSIZE / 4, BLUE);
  tft.fillRect(OPTION2_X, OPTIONS_Y, BOXSIZE, BOXSIZE / 4, MAGENTA);
  tft.setCursor(OPTION1_X + 50, OPTIONS_Y + 12);
  displayFromSD(_file, _line1, WHITE, 1);
  tft.setCursor(OPTION2_X + 50, OPTIONS_Y + 12);
  displayFromSD(_file, _line2, WHITE, 1);
}

bool leftOption() {
  // touchscreen is different from tft coordinates so bodge it
  // also x and y are different because of screen rotation
  bool y = p.x >= 150 && p.x <= 270;
  bool x = p.y >= 570 && p.y <= 900;
  return y && x;
}

bool rightOption() {
  // touchscreen is different from tft coordinates so bodge it
  // also x and y are different because of screen rotation
  bool y = p.x >= 150 && p.x <= 270;
  bool x = p.y >= 200 && p.y <= 520;
  return y && x;
}

void display(int _screen, String _file) {

  clickable = false;

  reset();

  hue = random(0, 255);

  tft.setCursor(0, 0);

  displayFromSD(_file, 1, GREEN, 1);
  displayFromSD(_file, 2, GREEN, 1);
  displayFromSD(_file, 3, YELLOW, 1);
  displayFromSD(_file, 4, BLUE, 1);
  displayFromSD(_file, 5, MAGENTA, 1);

  drawOptions(_file, 6, 7);

  screen = _screen;

  clickable = true;

}

String start_text = "Touch anywhere to start.";
String you_died = "YOU DIED";
void death(String _file, unsigned int line = 1) {

  hue = 95;

  screen = 0;
  reset();

  tft.setCursor(15, 25);
  write(you_died, RED, 6);

  tft.setCursor(0, 100);
  
  displayFromSD(_file, line, GREEN, 1);

  write(start_text, WHITE, 1);

}

String you_won1 = "MANNING";
String you_won2 = "ALWAYS WINS";
void win(String _file1, unsigned int _line1, String _file2, unsigned int _line2) {

  led_program = 1;

  screen = 0;
  reset();

  tft.setCursor(80, 10);
  write(you_won1, CYAN, 4);

  tft.setCursor(30, 50);
  write(you_won2, CYAN, 4);

  tft.setCursor(0, 95);

  displayFromSD(_file1, _line1, GREEN, 1);

  displayFromSD(_file2, _line2, GREEN, 1);

  write(start_text, WHITE, 1);

}
