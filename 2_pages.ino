void checkOptions() {

  // I dont know why the returns are necessary
  // but they are... without the, it just executes the next condition
  // even though that condition is false. makes no sense.
  
  // top right escape to title box
  // death screen, next move is reboot
  if ((p.x >= 800 && p.x <= 950 && p.y >= 150 && p.y <= 300) || screen == 0) {
    reboot();
    return;
  }

  // title screen to first page
  if (screen == 1) {
    led_program = 2;
    display(2, "start");
    return;
  }
  else {
    if (leftOption()) {
      // first page to hag death
      if (screen == 2) { death("hagdeath", 1); }
      if (screen == 3) { display(4, "library"); return; } // hag escape
      if (screen == 4) { study(); return; } // library
      if (screen == 5) { librarian(); return; } // study to follow librarian
      if (screen == 6) { pirates(); return; } // boat to fighting pirates
      if (screen == 7) { display(6, "boat"); return; } // librarian to boat
      if (screen == 8) { display(9, "bar"); return; } // skipton to bar
      if (screen == 9) { display( 11, "stranger"); return; } // bar to talk
      if (screen == 10) { shar(); return; } // church to shar
      if (screen == 11) { win("stranger", 9, "stranger", 10); return; }
    } 
    if (rightOption()) {
      if (screen == 2) { display(3, "hagescap"); return; }  // first page to hag escape
      if (screen == 3) { display(6, "boat"); return; }  // hag escape to boat
      if (screen == 4) { steal(); return; }  // library to steal
      if (screen == 5) { display(6, "boat"); return; }  // study to boat
      if (screen == 6) { display(8, "skipton"); return; }  // boat to skipton
      if (screen == 7) { display(8, "skipton"); return; }  // librarian to skipton
      if (screen == 8) { display(10, "church"); return; }  // skipton to church
      if (screen == 9) { barResult(); return; } // bar to play
      if (screen == 10) { churchLeave(); return; } // church to leave
      if (screen == 11) { barResult(); return; }
    }

  }

}

void study() {
  can_fight = true;
  display(5, "study");
}

void steal() { 
  stole = true;
  display(7, "steal");
}

void librarian() {
  can_fight = true;
  training_complete = true;
  display(7, "libraria");
}

void pirates() {
  if (can_fight) {
    win("pirates", 1, "pirates", 2);
  }
  else {
    death("pirates", 3);
  }
}

void barResult() {
  if (training_complete) {
    win("bar", 11, "bar", 12);
  }
  else {
    death("bar", 9);
  }
}

void shar() {
  if (stole) {
    win("church", 9, "church", 10);
  }
  else {
    win("church", 11, "church", 12);
  }
}

void churchLeave() {
  if (stole) {
    death("church", 14);
  }
  else {
    win("church", 16, "church", 17);
  }
}

void title() {

  String file = "title";
  led_program = 1;

  screen = 1;
  reset();

  tft.setCursor(15, 20);
  displayFromSD(file, 1, RED, 4);

  tft.setCursor(90, 70);
  displayFromSD(file, 2, WHITE, 2);

  tft.setCursor(15, 110);
  displayFromSD(file, 3, YELLOW, 7);

  tft.setCursor(5, 175);
  displayFromSD(file, 4, YELLOW, 4);

  tft.setCursor(80, 230);
  write(start_text, WHITE, 1);

}
