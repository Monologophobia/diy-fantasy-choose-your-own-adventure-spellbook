int hue = 0;
int brightness = 255;
unsigned long previousMillis = 0;
int led_program = 1;
bool direction = true;

void runLeds() {

  if (asyncDelay()) {
    if (led_program == 1) {
      brightness = 255;
      hue++;
      if (hue > 255) hue = 0;
    }
    else if (led_program == 2) {
      if (brightness >= 255) direction = false;
      if (brightness <= 0) direction = true;
      if (direction) {
        brightness++;
      }
      else {
        brightness--;
      }
    }
    for (int i = 0; i < NUM_LEDS; i++) {
      leds[i] = CHSV(hue, 255, brightness);
    }

    FastLED.show();
  }

}

bool asyncDelay() {
  unsigned long currentMillis = millis();
  if (currentMillis - previousMillis >= 100) {
    // save the last time you blinked the LED
    previousMillis = currentMillis;
    return true;
  }
  return false;
}
