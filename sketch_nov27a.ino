#include <Adafruit_GFX.h>    // Core graphics library
#include <Adafruit_TFTLCD.h> // Hardware-specific library
#include <TouchScreen.h>
#define FASTLED_INTERNAL // disable pragma message from library
#include "FastLED.h"
#include <SD.h>

#if defined(__SAM3X8E__)
    #undef __FlashStringHelper::F(string_literal)
    #define F(string_literal) string_literal
#endif

#define YP A1  // must be an analog pin, use "An" notation!
#define XM A2  // must be an analog pin, use "An" notation!
#define YM 7   // can be a digital pin
#define XP 6   // can be a digital pin

// display pin controls
#define LCD_CS A3
#define LCD_CD A2
#define LCD_WR A1
#define LCD_RD A0
// optional
#define LCD_RESET A4

// for options
#define BOXSIZE 130
#define OPTIONS_Y 200
#define OPTION1_X 20
#define OPTION2_X 170

// for LEDS
#define DATA_PIN 1
#define NUM_LEDS 4

// For better pressure precision, we need to know the resistance
// between X+ and X- Use any multimeter to read it
// For the one we're using, its 300 ohms across the X plate
TouchScreen ts = TouchScreen(XP, YP, XM, YM, 300);
Adafruit_TFTLCD tft(LCD_CS, LCD_CD, LCD_WR, LCD_RD, LCD_RESET);
CRGB leds[NUM_LEDS];

// set global variables
// what bit of the story are we at ?
int screen = -1; // what story page are we going to?
bool clickable = true;
TSPoint p; // touchscreen pointer

// story specific variables
bool can_fight = false;
bool stole = false; // naughty
bool training_complete = false;

void setup(void) {

  // start sdcard  
  while (!SD.begin(10));

  // start leds
  FastLED.addLeds<WS2812B, DATA_PIN, RGB>(leds, NUM_LEDS);

  // fire up the screen
  tft.reset();
  uint16_t identifier = tft.readID();
  tft.begin(identifier);
  tft.setRotation(3);

  pinMode(13, OUTPUT);

  // display the title
  title();

}

// give us some boundaries for the touch
#define MINPRESSURE 750
#define MAXPRESSURE 800

void loop() {

  // get the touched point
  digitalWrite(13, HIGH);
  p = ts.getPoint();
  digitalWrite(13, LOW);

  pinMode(XM, OUTPUT);
  pinMode(YP, OUTPUT);

  // only register a single touch. wait for the screen to render before listening to touches again
  if (clickable) {

    // we have some minimum pressure we consider 'valid'
    // pressure of 0 means no pressing!
    if (p.z > MINPRESSURE && p.z < MAXPRESSURE) {
      checkOptions();
      delay(100); // ?
    }

  }

  runLeds();

}
